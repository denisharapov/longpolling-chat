import React, {Component} from 'react';
import './App.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPaperPlane, faImage  } from '@fortawesome/free-solid-svg-icons';
import NodeRSA from 'node-rsa';

let serverURL = "http://localhost:10010";

class App extends Component {

    constructor(props) {
        super(props);

        this.state = {
            messages: [],
            messagesCount: 0,
            userMessageText: "",
            username: "",
            token: "",
            usernameText: "",
            passwordText: "",
            passwordConfText: "",
            privateKey: "",
            publicKey: "",
            serverKey: "",
            isRegister: false
        };
    }

    messagesListener() {
        console.log('Sending get messages request');
        fetch(serverURL + "/messages?l=" + this.state.messagesCount + '&token=' + this.state.token)
            .then((response) => {
                return response.json();
            }).then((messages) => {
                const tempKey = new NodeRSA(this.state.privateKey);
                messages = tempKey.decrypt(messages, 'json');

                this.setState({
                    messages: messages,
                    messagesCount: messages.length
                }, this.scrollToBottom);

                this.messagesListener();
            });
    }

    componentDidMount() {
        let token = sessionStorage.getItem("token");

        if(!token){
            this.setState({
               token: null
            });
            return
        }

        this.setState({
            token: token
        });

        this.messagesListener();
    }

    sendMessage = () => {
        const tempKey = new NodeRSA();
        tempKey.importKey(this.state.serverKey, 'pkcs8-public-pem');
        var data = JSON.stringify({
            text: tempKey.encrypt(this.state.userMessageText.replace(/(?:\r\n|\r|\n)/g, '<br>'), 'base64'),
            time: new Date(),
            token: this.state.token
        });


        if(this.state.userMessageText !== "") {
            fetch(serverURL + '/messages', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: data
            }).then((data) => {
                this.setState({
                    userMessageText: ""
                });
            });
        }
    };

    scrollToBottom = () => {
        this.messagesEnd.scrollIntoView({ behavior: "smooth" });
    };

    getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    }

    formatDate(date) {
        let d = new Date(date);
        let dateString = "";
        let hours = d.getHours();
        let minutes = d.getMinutes();
        hours = (hours < 10) ? '0' + hours : hours;
        minutes = (minutes < 10) ? '0' + minutes : minutes;
        dateString = `${hours}:${minutes}`;
        return dateString;
    }

    onEnterPress = (e) => {
        if(e.keyCode === 13 && e.shiftKey === false) {
            this.sendMessage();
        }
    };

    handleSubmit = (event) => {
        event.preventDefault();
        const key = new NodeRSA({b: 512});
        const publicDer = key.exportKey('public');
        const privateDer = key.exportKey('private');

        this.setState({
            publicKey: publicDer,
            privateKey: privateDer
        });

        fetch(serverURL + '/user/login', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name: this.state.usernameText,
                password: this.state.passwordText,
                key: publicDer
            })
        }).then((response) => {
            if(response.status === 404) {
                alert("User not found");
                return {err: 404};
            }
            return response.json();
        }).then((data) => {
            if(data.err === 404)
                return;

            this.setState({
                token: data.token,
                serverKey: data.key,
                username: this.state.usernameText
            });
            this.messagesListener();
        }).catch((err) => {
            console.log(err);
        });
    };

    handleRegister = (event) => {
        event.preventDefault();

        if(this.state.passwordText !== this.state.passwordConfText) {
            alert("Passwords do not match!");
        }

        fetch(serverURL + '/user', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name: this.state.usernameText,
                password: this.state.passwordText
            })
        }).then((response) => {
            if(response.status === 404) {
                alert("User already exists!");
                return {err: 404};
            }
            return response.json();
        }).then((data) => {
            if(data.err === 404)
                return;

            if(data === "ok") {
                alert("User created! Now you can sing in.");
            }
        }).catch((err) => {
            console.log(err);
        });
    };

    handleFile = (files) => {
        console.log(files);
        var formData  = new FormData();
        if(files.length === 0)
            return;
        if(files[0].type !== "image/jpeg" && files[0].type !== "image/png") {
            alert("Only JPG or PNG allowed!");
            return;
        }
        formData.append('photo', files[0]);
        formData.append('token', this.state.token);
        fetch(serverURL + '/messages/photo', { // Your POST endpoint
            method: 'POST',
            body: formData, // This is your file object
        }).then(
            response => response.json() // if the response is a JSON object
        ).then(
            success => console.log(success) // Handle the success response object
        ).catch(
            error => console.log(error) // Handle the error response object
        );
    };

    render() {
        let overlay = null;

        if(this.state.isRegister) {
            overlay =
                <div className="modal-overlay">
                    <div className="modal">
                        <form onSubmit={this.handleSubmit}>
                            <input type="text" id="username" placeholder="Username"
                                   onChange={(e) => {this.setState({usernameText: e.target.value})}}
                                   value={this.state.usernameText}/>
                            <input type="password" id="password" placeholder="Password"
                                   onChange={(e) => {this.setState({passwordText: e.target.value})}}
                                   value={this.state.passwordText}/>
                            <input type="password" id="passwordConf" placeholder="Confirm password"
                                   onChange={(e) => {this.setState({passwordConfText: e.target.value})}}
                                   value={this.state.passwordConfText}/>
                            <button onClick={this.handleRegister}>Register</button>
                            <a onClick={() => { this.setState({isRegister: false}) }} href="#">Sing in</a>
                        </form>
                    </div>
                </div>;
        }

        if(!this.state.token && !this.state.isRegister) {
            overlay =
                <div className="modal-overlay">
                    <div className="modal">
                        <form onSubmit={this.handleSubmit}>
                            <input type="text" id="username" placeholder="Username"
                                   onChange={(e) => {this.setState({usernameText: e.target.value})}}
                                   value={this.state.usernameText}/>
                            <input type="password" id="password" placeholder="Password"
                                   onChange={(e) => {this.setState({passwordText: e.target.value})}}
                                   value={this.state.passwordText}/>
                            <button type="submit">Sing in</button>
                            <a onClick={() => { this.setState({isRegister: true}) }} href="#">Register</a>
                        </form>
                    </div>
                </div>;
        }
        return (
            <div className="App">
                {overlay}
                <div className="App-chat-container">
                    <div className="chat-box">
                        <div className="messages-container">
                            <ul className="messagesList">
                                {
                                    this.state.messages.map((item, ind) => {
                                        return(
                                            <li key={"m" + ind} className={(item.username === this.state.username) ? 'mine' : ''}>
                                                <span className="username">{item.username}</span>
                                                <div className="bubble" dangerouslySetInnerHTML={{ __html: item.message}}/>
                                                <span className="time">{this.formatDate(item.time)}</span>
                                            </li>
                                        );
                                    })
                                }
                            </ul>
                            <div style={{ float:"left", clear: "both" }}
                                 ref={(el) => { this.messagesEnd = el; }}>
                            </div>
                        </div>
                        <div className="input-container">
                            <textarea
                                onKeyDown={this.onEnterPress}
                                placeholder={"Message..."}
                                onChange={(e) => {this.setState({userMessageText: e.target.value})}}
                                value={this.state.userMessageText}/>
                            <label>
                                <input className="fileInput" type="file" onChange={ (e) => this.handleFile(e.target.files) }/>
                                <div className="uploadFileBtn">
                                    <FontAwesomeIcon icon={faImage} size="lg" />
                                </div>
                            </label>
                            <button onClick={() => {this.sendMessage()}}>
                                <FontAwesomeIcon icon={faPaperPlane} size="lg" />
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default App;
